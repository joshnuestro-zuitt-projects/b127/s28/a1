//Number 3

fetch('https://jsonplaceholder.typicode.com/todos')
.then(res => res.json())
.then(data => console.log(data))

//Number 4

fetch('https://jsonplaceholder.typicode.com/todos')
.then(res => res.json())
.then(data => {
	let list = data.map(post => {
		return post.title
	})
	console.log(list)
})

//Number 5

fetch('https://jsonplaceholder.typicode.com/todos/5')
.then(res => res.json())
.then(data => console.log(data))

//Number 6

fetch('https://jsonplaceholder.typicode.com/todos?id=6')
.then(res => res.json())
.then(data => {
	let list = data.map(post => {
		return `${post.title}, ${post.completed}`
	})
	console.log(list)
})

//Number 7

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 7,
		title: 'New Post',
		completed: true
	})
})
.then(res => res.json())
.then(data => console.log(data))

//Number 8

fetch('https://jsonplaceholder.typicode.com/posts/8', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
	userId: 8,
	title: "The Legend of the Condor Hero",
	completed: true
	})
})
.then(res => res.json())
.then(data => console.log(data))

//Number 9

fetch('https://jsonplaceholder.typicode.com/posts/9', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
	title: "Jojo's Bizarre Adventure",
	description: "The best anime out there.",
	status: true,
	dateCompleted: "2012-04-21T18:25:43-05:00",
	userId: 9
	})
})
.then(res => res.json())
.then(data => console.log(data))

//Number 10

fetch('https://jsonplaceholder.typicode.com/posts/10', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
	completed: true
	})
})
.then(res => res.json())
.then(data => console.log(data))

//Number 11

fetch('https://jsonplaceholder.typicode.com/posts/11', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
	completed: true,
	updated: "2021-09-21T22:03:43-05:00"
	})
})
.then(res => res.json())
.then(data => console.log(data))

//Number 12

fetch('https://jsonplaceholder.typicode.com/posts/12', {
	method: 'DELETE'
})
.then(res => res.json())
.then(data => console.log(data))

